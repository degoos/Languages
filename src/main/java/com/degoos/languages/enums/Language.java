package com.degoos.languages.enums;

import com.degoos.languages.Languages;
import com.degoos.languages.manager.FileManager;
import com.degoos.languages.object.LanguageMap;
import com.degoos.languages.util.FileUtils;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.util.Validate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public enum Language {

    AFRIKAANS("Afrikaans", "af_ZA"),
    ARABIC("Arabic", "ar_SA"),
    ASTURIAN("Asturian", "ast_ES"),
    AZERBAIJANI("Azerbaijani", "az_AZ"),
    BELARUSIAN("Belarusian", "be_BY"),
    BULGARIAN("Bulgarian", "bg_BG"),
    BRETON("Breton", "br_FR"),
    CATALAN("Catalan", "ca_ES"),
    CZECH("Czech", "cs_CZ"),
    WELSH("Welsh", "cy_GB"),
    DANISH("Danish", "da_DK"),
    AUSTIAN_GERMAN("Austrian German", "de_AT"),
    GERMAN("German", "de_DE"),
    GREEK("Greek", "el_GR"),
    AUSTRIALAN_ENGLISH("Australian English", "en_AU"),
    CANADIAN_ENGLISH("Canadian English", "en_CA"),
    BRITISH_ENGLISH("British English", "en_GB"),
    NEW_ZEALAND_ENGLISH("New Zealand English", "en_NZ"),
    BRITISH_ENGLISH_UPSIDE_DOWN("British English Upside Down", "en_UD"),
    PIRATE_ENGLISH("Pirate English", "en_7S"),
    AMERICAN_ENGLISH("American English", "en_US"),
    ESPERANTO("Esperanto", "eo_UY"),
    ARGENTINIAN_SPANISH("Argentinian Spanish", "es_AR"),
    SPANISH("Spanish", "es_ES"),
    MEXICAN_SPANISH("Mexican Spanish", "es_MX"),
    URUGUAYAN_SPANISH("Uruguayan Spanish", "es_UY"),
    VENEZUELAN_SPANISH("Venezuelan Spanish", "es_VE"),
    ESTONIAN("Estonian", "et_EE"),
    BASQUE("Basque", "eu_ES"),
    PERSIAN("Persian", "fa_IR"),
    FINNISH("Finnish", "fi_FI"),
    FILIPINO("Filipino", "fil_PH"),
    FAROESE("Faroese", "fo_FO"),
    FRENCH("French", "fr_FR"),
    CANADIAN_FRENCH("Canadian French", "fr_CA"),
    FRISIAN("Frisian", "fy_NL"),
    IRISH("Irish", "ga_IE"),
    SCOTTISH_GAELIC("Scottish Gaelic", "gd_GB"),
    GALICIAN("Galician", "gl_ES"),
    MANX("Manx", "gv_IM"),
    HAWAIIAN("Hawaiian", "haw"),
    HEBREW("Hebrew", "he_IL"),
    HINDI("Hindi", "hi_IN"),
    CROATIAN("Croatian", "hr_HR"),
    IDO("Ido", "io"),
    ITALIAN("Italian", "it_IT"),
    JAPANESE("Japanese", "ja_JP"),
    LOJBAN("Lojban", "jbo_EN"),
    GERGIAN("Georgian", "ka_GE"),
    KOREAN("Korean", "ko_KR"),
    RIPUARIAN("Ripuarian", "ksh_DE"),
    CORNISH("Cornish", "kw_GB"),
    LATIN("Latin", "la_VA"),
    LUXEMBOURGISH("Luxembourgish", "lb_LU"),
    LIMBURGISH("Limburgish", "li_LI"),
    LOLCAT("LOLCAT", "lol_US"),
    LITHUANIAN("Lithuanian", "lt_LT"),
    LATVIAN("Latvian", "lv_LV"),
    MAORI("Maori", "mi_NZ"),
    MACEDONIAN("Macedonian", "mk_MK"),
    MONGOLIAN("Mongolian", "mn_MN"),
    MALAY("Malay", "ms_MY"),
    MALTESE("Maltese", "mt_MT"),
    LOW_GERMAN("Low German", "nds_DE"),
    DUTCH("Dutch", "nl_NL"),
    NORWEGIAN_NYNORSK("Norwegian Nynorsk", "nn_NO"),
    NORWEGIAN("Norwegian", "no_NO"),
    OCCITAN("Occitan", "oc_FR"),
    POLISH("Polish", "pl_PL"),
    BRAZILIAN_PORTUGUESE("Brazilian Portuguese", "pt_BR"),
    PORTUGUESE("Portuguese", "pt_PT"),
    QUENYA("Quanya", "qya_AA"),
    ROMANIAN("Romanian", "ro_RO"),
    RUSSIAN("Russian", "ru_RU"),
    NORTHERN_SAMI("Northern Sami", "sme"),
    SLOVAK("Slovak", "sk_SK"),
    SLOVENIAN("Slovenian", "sl_SL"),
    SOMALI("Somali", "so_SO"),
    ALBANIAN("Albanian", "sq_AL"),
    SERBIAN("Serbian", "sr_SP"),
    SWEDISH("Swedish", "sv_SE"),
    SWABIAN_GERMAN("Swabian German", "swg_DE"),
    THAI("Thai", "th_TH"),
    FILIPINO_2("Filipino", "tl_PH"),
    KLINGON("Klingon", "tlh_AA"),
    TURKISH("Turkish", "tr_TR"),
    TALOSSAN("Talossan", "tzl_TZL"),
    UKRANIAN("Ukranian", "uk_UA"),
    VALENCIAN("Valencian", "ca-val_ES"),
    VIETNAMESE("Vietnamese", "vi_VN"),
    SIMPLIFIED_CHINESE("Simplified Chinese", "zh_CN"),
    TRADITIONAL_CHINESE("Traditional Chinese", "zh_TW");


    private String defaultName, localeCode;
    private Set<String> names;
    private Map<String, LanguageMap> messages;

    Language(String defaultName, String localeCode) {
        this.defaultName = defaultName;
        this.localeCode = localeCode;
        this.names = new HashSet<>();
        this.messages = new HashMap<>();
    }

    public String getDefaultName() {
        return defaultName;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public Set<String> getNames() {
        return new HashSet<>(names);
    }

    public void addName(String name) {
        names.add(name);
    }

    public void removeName(String name) {
        names.remove(name);
    }

    public boolean hasPlugin(WSPlugin plugin) {
        return messages.containsKey(plugin.getId());
    }

    public Optional<LanguageMap> getLanguageMap(WSPlugin plugin) {
        return Optional.ofNullable(messages.get(plugin.getId()));
    }

    public LanguageMap getLanguageMapUnchecked(WSPlugin plugin) {
        return messages.get(plugin.getId());
    }

    public boolean loadPlugin(WSPlugin plugin) {
        Validate.notNull(plugin, "Plugin cannot be null!");
        messages.remove(plugin.getId());

        InputStream in = plugin.getResource(localeCode + ".yml");

        File pluginFolder = new File(Languages.getManager(FileManager.class).getPluginFilesFolder(), plugin.getId());
        FileUtils.checkFolder(pluginFolder);

        Optional<File> optional = Arrays.stream(pluginFolder.listFiles()).filter(langFile -> langFile.getName().equals(localeCode + ".yml")).findAny();

        if (!optional.isPresent()) {
            if (in == null) return false;
            File langFile = new File(pluginFolder, localeCode + ".yml");
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(langFile);
                int read;
                byte[] bytes = new byte[1024];

                while ((read = in.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
            } catch (Exception e) {
                return false;
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }
            messages.put(plugin.getId(), new LanguageMap(plugin, new ConfigAccessor(langFile)));
        } else {
            ConfigAccessor fileConfig = new ConfigAccessor(optional.get());
            if (in != null) checkNodes(in, fileConfig);
            if (!localeCode.equals("en_US")) {
                InputStream englishInput = plugin.getResource("en_US.yml");
                if (englishInput != null) checkNodes(englishInput, fileConfig);
            }
            LanguageMap languageMap = new LanguageMap(plugin, fileConfig);
            messages.put(plugin.getId(), languageMap);
        }
        return true;
    }

    public void loadNames() {
        names.clear();
        names.add(defaultName);
        names.add(localeCode);
        Arrays.stream(values()).forEach(language ->
                language.getLanguageMap(Languages.getInstance()).ifPresent(languageMap -> languageMap.getMessage("language." + localeCode).ifPresent(names::add)));
    }

    private void checkNodes(InputStream in, ConfigAccessor fileConfig) {
        ConfigAccessor classConfig = new ConfigAccessor(in);
        classConfig.getKeys(true).stream().filter(key -> !fileConfig.contains(key)).forEach(key -> fileConfig.set(key, classConfig.getString(key)));
        fileConfig.getKeys(true).stream().filter(key -> !classConfig.contains(key)).forEach(key -> fileConfig.set(key, null));
        fileConfig.save();
    }

    public static Optional<Language> getByName(String name) {
        return Arrays.stream(values()).filter(language -> language.getNames().stream().anyMatch(n -> n.equalsIgnoreCase(name))).findAny();
    }


    public static Optional<Language> getByDefaultName(String name) {
        return Arrays.stream(values()).filter(language -> language.getDefaultName().equalsIgnoreCase(name)).findAny();
    }

    public static Optional<Language> getByLocaleCode(String code) {
        return Arrays.stream(values()).filter(language -> language.getLocaleCode().equalsIgnoreCase(code)).findAny();
    }

    public static Set<String> getAllNames() {
        Set<String> set = new HashSet<>();
        Arrays.stream(values()).forEach(language -> set.addAll(language.getNames()));
        return set;
    }
}
