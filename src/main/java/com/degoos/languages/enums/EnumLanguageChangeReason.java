package com.degoos.languages.enums;

public enum  EnumLanguageChangeReason {

    JOIN, COMMAND, CUSTOM;

}
