package com.degoos.languages.util;

import java.io.File;

public class FileUtils {

    public static void checkFolder(File file) {
        if (!file.exists()) file.mkdirs();
        else if (file.isFile()) {
            file.delete();
            file.mkdirs();
        }
    }

}
