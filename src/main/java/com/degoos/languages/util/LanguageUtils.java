package com.degoos.languages.util;

import com.degoos.languages.Languages;
import com.degoos.languages.enums.Language;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.FileManager;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import java.util.Arrays;

public class LanguageUtils {

	private static boolean checked = false, usePlaceholder = false;

	public static Language getNearestCompatibleLanguage(Language language, WSPlugin plugin) {
		if (language.hasPlugin(plugin)) return language;
		ConfigAccessor secondary = ManagerLoader.getManager(FileManager.class).getSecondary();
		Language second = Language.getByLocaleCode(secondary.getString(language.getLocaleCode(), "en_US")).orElse(Languages.getDefaultLanguage());
		if (second.hasPlugin(plugin)) return second;
		Language third = Language.getByLocaleCode(secondary.getString(second.getLocaleCode(), "en_US")).orElse(Languages.getDefaultLanguage());
		if (third.hasPlugin(plugin)) return third;
		if (Languages.getDefaultLanguage().hasPlugin(plugin)) return Languages.getDefaultLanguage();
		return Arrays.stream(Language.values()).filter(target -> target.hasPlugin(plugin)).findAny().orElseThrow(NullPointerException::new);
	}

	public static WSText factMessage(String message, Language language, WSPlugin plugin, boolean prefix, boolean center, WSPlayer player, Object... args) {

		if (!checked) {
			try {
				Class.forName("com.degoos.wetsponge.hook.placeholderapi.WSPlaceholderAPI");
				usePlaceholder = true;
			} catch (ClassNotFoundException e) {
				usePlaceholder = false;
			}
		}

		for (int i = 0; i < args.length - 1; i += 2)
			message = message.replace(args[i].toString(), args[i + 1] instanceof WSText ? ((WSText) args[i + 1]).toFormattingText() : args[i + 1].toString());
		if (prefix) message = language.getLanguageMapUnchecked(plugin).getMessage("prefix").orElse("") + message;
		WSText.Builder builder = WSText.builder(message).translateColors();

		WSText factorizedMessage = center ? builder.center().build() : builder.build();

		return usePlaceholder ? PlaceholderAPIUtils. parse(factorizedMessage, player) : factorizedMessage;
	}

}
