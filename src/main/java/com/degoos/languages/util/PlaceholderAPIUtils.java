package com.degoos.languages.util;

import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.hook.WSHookManager;
import com.degoos.wetsponge.hook.placeholderapi.WSPlaceholderAPI;
import com.degoos.wetsponge.text.WSText;

public class PlaceholderAPIUtils {

	public static WSText parse(WSText text, WSPlayer player) {
		WSPlaceholderAPI api = WSHookManager.getInstance().getHook(WSPlaceholderAPI.class).orElse(null);
		if (api == null || player == null) return text;
		else return api.parse(player, text);
	}

}
