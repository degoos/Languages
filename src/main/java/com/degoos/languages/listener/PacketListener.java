package com.degoos.languages.listener;

import com.degoos.languages.api.LanguagesAPI;
import com.degoos.languages.enums.Language;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.PlayerManager;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerSendPacketEvent;
import com.degoos.wetsponge.item.WSItemStack;
import com.degoos.wetsponge.packet.play.server.WSSPacketSetSlot;
import com.degoos.wetsponge.packet.play.server.WSSPacketWindowItems;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;

import java.util.*;

public class PacketListener {

	private static Map<String, String> replace = new HashMap<>();

	@WSListener
	public void onPacketSend(WSPlayerSendPacketEvent event) {
		if (event.getPacket() instanceof WSSPacketWindowItems) {
			Language language = ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer(event.getPlayer()).getLanguage();
			if (((WSSPacketWindowItems) event.getPacket()).getItemStacks() != null)
				((WSSPacketWindowItems) event.getPacket()).getItemStacks().forEach(item -> updateItem(item, language));
		} else if (event.getPacket() instanceof WSSPacketSetSlot) {
			Language language = ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer(event.getPlayer()).getLanguage();
			updateItem(((WSSPacketSetSlot) event.getPacket()).getItemStack(), language);
		}
	}

	private static void updateItem(WSItemStack item, Language language) {
		if (item == null || item.getDisplayName() == null || item.getDisplayName().toPlain().isEmpty()) return;
		translateText(item.getDisplayName() == null ? null : item.getDisplayName().toPlain(), language).ifPresent(item::setDisplayName);
		List<WSText> lore = new ArrayList<>();
		item.getLore().forEach(text -> {
			List<WSText> list = translateTexts(text.toPlain(), language);
			if (!list.isEmpty()) lore.addAll(list);
			else lore.add(text);
		});
		item.setLore(lore);
		item.update();
	}

	static Optional<WSText> translateText(String text, Language language) {
		if (text == null || text.isEmpty()) return Optional.empty();
		if (!(text.startsWith("<Languages;") || text.startsWith("<L;")) || !text.endsWith(">")) return Optional.empty();

		if (replace.containsKey(text)) text = replace.get(text);

		String[] sl = text.substring(0, text.length() - 1).replaceFirst("<L;", "").replaceFirst("<Languages;", "").split(";");
		if (sl.length < 2) return Optional.empty();
		WSPlugin plugin = WetSponge.getPluginManager().getPlugin(sl[1]).orElse(null);
		if (plugin == null) return Optional.empty();
		Object[] replacements = sl.length < 3 ? new String[0] : Arrays.copyOfRange(sl, 2, sl.length - 1);
		return LanguagesAPI.getMessage(language, sl[0], false, plugin, replacements);
	}

	private static List<WSText> translateTexts(String text, Language language) {
		if (text == null || text.isEmpty()) return new ArrayList<>();
		if (!(text.startsWith("<Languages;") || text.startsWith("<L;")) || !text.endsWith(">")) return new ArrayList<>();

		if (replace.containsKey(text)) text = replace.get(text);

		String[] sl = text.substring(0, text.length() - 1).replaceFirst("<L;", "").replaceFirst("<Languages;", "").split(";");
		if (sl.length < 2) return new ArrayList<>();
		WSPlugin plugin = WetSponge.getPluginManager().getPlugin(sl[1]).orElse(null);
		if (plugin == null) return new ArrayList<>();
		Object[] replacements = sl.length < 3 ? new String[0] : Arrays.copyOfRange(sl, 2, sl.length - 1);
		return LanguagesAPI.getMessages(language, sl[0], false, plugin, replacements);
	}

	public static void addShortcut(String key, String value) {
		replace.put(key, value);
	}

	public static void removeShortcut(String key) {
		replace.remove(key);
	}
}
