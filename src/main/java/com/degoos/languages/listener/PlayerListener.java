package com.degoos.languages.listener;

import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.PlayerManager;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerJoinEvent;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerQuitEvent;

public class PlayerListener {

    @WSListener
    public void onPlayerJoin(WSPlayerJoinEvent event) {
        ManagerLoader.getManager(PlayerManager.class).addPlayer(event.getPlayer());
    }

    @WSListener
    public void onPlayerLeave(WSPlayerQuitEvent event) {
        ManagerLoader.getManager(PlayerManager.class).removePlayer(event.getPlayer());
    }

}
