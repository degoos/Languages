package com.degoos.languages.listener;

import com.degoos.languages.enums.Language;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.PlayerManager;
import com.degoos.wetsponge.event.WSListener;
import com.degoos.wetsponge.event.entity.player.connection.WSPlayerSendPacketEvent;
import com.degoos.wetsponge.packet.play.server.WSSPacketOpenWindow;

public class Packet2Listener {

	@WSListener
	public void onPacketSend(WSPlayerSendPacketEvent event) {
		if (event.getPacket() instanceof WSSPacketOpenWindow) {
			Language language = ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer(event.getPlayer()).getLanguage();
			PacketListener.translateText(((WSSPacketOpenWindow) event.getPacket()).getWindowTitle().toPlain(), language).ifPresent(((WSSPacketOpenWindow)
					event.getPacket())::setWindowTitle);
		}
	}

}
