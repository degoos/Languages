package com.degoos.languages.event;

import com.degoos.languages.enums.EnumLanguageChangeReason;
import com.degoos.languages.enums.Language;
import com.degoos.languages.object.LanguagePlayer;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.event.WSCancellable;
import com.degoos.wetsponge.event.WSEvent;

public class LanguageChangeEvent extends WSEvent implements WSCancellable {

    private EnumLanguageChangeReason reason;
    private WSTransaction<Language> transaction;
    private LanguagePlayer player;
    private boolean cancelled;

    public LanguageChangeEvent(EnumLanguageChangeReason reason, WSTransaction<Language> transaction, LanguagePlayer player) {
        this.reason = reason;
        this.transaction = transaction;
        this.player = player;
        this.cancelled = false;
    }


    public EnumLanguageChangeReason getReason() {
        return reason;
    }

    public WSTransaction<Language> getTransaction() {
        return transaction;
    }

    public LanguagePlayer getPlayer() {
        return player;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }
}
