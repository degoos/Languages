package com.degoos.languages.manager;


import com.degoos.languages.Languages;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.enums.EnumServerVersion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManager implements Manager {

	private static String username;
	private static String password;
	private static String url;


	public void load () {
		ConfigAccessor config = ManagerLoader.getManager(FileManager.class).getConfig();

		username = config.getString("database.username", "root");
		password = config.getString("database.password", "");
		url = replaceDatabaseString(config.getString("database.url", "jdbc:sqlite:{DIR}{NAME}.db"));

		try (Connection connection = DriverManager.getConnection(url, username, password)) {
			this.createDB(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void createDB (Connection connection) {
		String query = "CREATE TABLE IF NOT EXISTS " +
		               Languages.TABLE +
		               "(" +
		               "  UUID        VARCHAR(255) PRIMARY KEY," +
		               "  playerName  VARCHAR(255)," +
		               "  langCode    VARCHAR(10)" +
		               ");";
		try {
			Statement stmt = connection.createStatement();
			stmt.execute(query);
			DatabaseManager.sendCommit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public static Connection getConnection () {
		try {
			return DriverManager.getConnection(url, username, password);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static void sendCommit () {
		Connection connection = DatabaseManager.getConnection();
		if (connection == null) return;

		try {
			if (!connection.getAutoCommit()) connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	private String replaceDatabaseString (String input) {
		input = input.replaceAll("\\{DIR\\}", Languages.getInstance().getDataFolder().getPath().replaceAll("\\\\", "/") + "/");
		input = input.replaceAll("\\{NAME\\}", Languages.getInstance().getId());

		return input;
	}
}
