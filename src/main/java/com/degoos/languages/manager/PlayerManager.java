package com.degoos.languages.manager;


import com.degoos.languages.Languages;
import com.degoos.languages.enums.Language;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.object.LangPlayerRecord;
import com.degoos.languages.object.LanguagePlayer;
import com.degoos.languages.object.LoginPlayer;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

public class PlayerManager implements Manager {

	private Set<LanguagePlayer> players;
	private Set<LoginPlayer> loginPlayers;


	@Override
	public void load() {
		players = new HashSet<>();
		loginPlayers = new HashSet<>();
	}


	public Set<LanguagePlayer> getPlayers() {
		return new HashSet<>(players);
	}


	public void addPlayer(LanguagePlayer player) {
		players.add(player);
	}

	private void loadLoginPlayer(UUID uuid) {
		LoginPlayer loginPlayer = new LoginPlayer(Languages.getDefaultLanguage(), false, uuid, null);
		loginPlayers.add(loginPlayer);
		Connection connection = DatabaseManager.getConnection();
		if (connection == null) throw new NullPointerException("Connection is null");
		DSLContext create = DSL.using(connection);
		LangPlayerRecord langPlayerRecord = create.select().from(Languages.TABLE).where("UUID = '" + uuid + "'").fetchOneInto(LangPlayerRecord.class);
		if (langPlayerRecord != null) {
			loginPlayer.setLanguage(Language.getByLocaleCode(langPlayerRecord.langCode).orElse(Languages.getDefaultLanguage()));
			loginPlayer.setRecord(langPlayerRecord);
			loginPlayer.setFullyLoaded(true);
		}
	}

	public LanguagePlayer addPlayer(WSPlayer player) {
		Optional<LoginPlayer> optional = loginPlayers.stream().filter(loginPlayer -> loginPlayer.getUuid().equals(player.getUniqueId())).findAny();
		if (optional.isPresent()) {
			LoginPlayer loginPlayer = optional.get();
			loginPlayers.remove(loginPlayer);
			if (loginPlayer.isFullyLoaded()) {
				LanguagePlayer languagePlayer = new LanguagePlayer(loginPlayer.getRecord(), player, loginPlayer.getLanguage());
				players.add(languagePlayer);
				return languagePlayer;
			}
		}

		boolean getMinecraftLanguage = ManagerLoader.getManager(FileManager.class).getConfig().getBoolean("getMinecraftLanguageOnFirstJoin", true);
		LanguagePlayer languagePlayer = new LanguagePlayer(null, player,
			getMinecraftLanguage ? Language.getByLocaleCode(player.getLanguageCode()).orElse(Languages.getDefaultLanguage()) : Languages.getDefaultLanguage());
		players.add(languagePlayer);
		Connection connection = DatabaseManager.getConnection();
		if (connection == null) throw new NullPointerException("Connection is null");
		DSLContext create = DSL.using(connection);
		LangPlayerRecord langPlayerRecord = create.select().from(Languages.TABLE).where("UUID = '" + player.getUniqueId() + "'").fetchOneInto(LangPlayerRecord.class);
		if (langPlayerRecord != null) langPlayerRecord.createLanguagePlayer(languagePlayer);
		else new LangPlayerRecord(player.getUniqueId().toString(), player.getName(),
			getMinecraftLanguage ? player.getLanguageCode() : Languages.getDefaultLanguage().getLocaleCode()).createLanguagePlayer(languagePlayer);
		DatabaseManager.sendCommit();

		return languagePlayer;
	}


	public void removePlayer(LanguagePlayer player) {
		players.remove(player);
	}


	public void removePlayer(WSPlayer player) {
		new ArrayList<>(players).stream().filter(languagePlayer -> languagePlayer.getPlayer().equals(player)).findAny().ifPresent(PlayerManager.this::removePlayer);
	}


	public LanguagePlayer getOrCreatePlayer(WSPlayer player) {
		return players.stream().filter(target -> target.getPlayer().equals(player)).findAny().orElse(addPlayer(player));
	}


	public Optional<LanguagePlayer> getPlayer(WSPlayer player) {
		return players.stream().filter(target -> target.getPlayer().equals(player)).findAny();
	}


	public Optional<LanguagePlayer> getPlayer(String name) {
		return players.stream().filter(player -> player.getPlayer().getName().equals(name)).findAny();
	}


	public Optional<LanguagePlayer> getPlayer(UUID uuid) {
		return players.stream().filter(player -> player.getPlayer().getUniqueId().equals(uuid)).findAny();
	}
}
