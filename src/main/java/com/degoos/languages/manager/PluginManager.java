package com.degoos.languages.manager;

import com.degoos.languages.Languages;
import com.degoos.languages.enums.Language;
import com.degoos.languages.util.PluginUtils;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.plugin.WSPlugin;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PluginManager implements Manager {

	private Map<WSPlugin, Set<Language>> plugins;

	public void load() {
		plugins = new HashMap<>();
		WetSponge.getPluginManager().getPlugins().stream().filter(
			plugin -> plugin.getPluginDescription().getDepend().contains("Languages") || plugin.getPluginDescription().getSoftDepend().contains("Languages") ||
				plugin.equals(Languages.getInstance())).forEach(this::addPlugin);
		Arrays.stream(Language.values()).forEach(Language::loadNames);
	}

	public void addPlugin(WSPlugin plugin) {
		Set<Language> set = new HashSet<>();
		Arrays.stream(Language.values()).filter(language -> language.loadPlugin(plugin)).forEach(set::add);
		if (set.isEmpty()) return;
		plugins.put(plugin, set);
	}

	public void removePlugin(WSPlugin plugin) {
		plugins.remove(plugin);
	}

	public boolean hasPluginLanguage(WSPlugin plugin, Language language) {
		return plugins.containsKey(plugin) && plugins.get(plugin).contains(language);
	}

	public boolean hasPlugin(WSPlugin plugin) {
		return plugins.containsKey(plugin);
	}

	public Map<WSPlugin, Set<Language>> getPlugins() {
		return new HashMap<>(plugins);
	}
}
