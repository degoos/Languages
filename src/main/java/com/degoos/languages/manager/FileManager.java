package com.degoos.languages.manager;

import com.degoos.languages.Languages;
import com.degoos.languages.util.FileUtils;
import com.degoos.wetsponge.config.ConfigAccessor;

import java.io.File;
import java.io.InputStream;

public class FileManager implements Manager {

    private File pluginFilesFolder;
    private ConfigAccessor config;
    private ConfigAccessor secondary;

    public void load() {
        File pluginFolder = Languages.getInstance().getDataFolder();
        pluginFilesFolder = new File(pluginFolder, "PluginFiles");
        FileUtils.checkFolder(pluginFilesFolder);
        config = new ConfigAccessor(new File(pluginFolder, "config.yml"));
        secondary = new ConfigAccessor(new File(pluginFolder, "secondary.yml"));
        checkNodesFromJarFile("languagesConfig.yml", config);
        checkNodesFromJarFile("secondary.yml", secondary);
    }


    public File getPluginFilesFolder() {
        return pluginFilesFolder;
    }

    public ConfigAccessor getConfig() {
        return config;
    }

    public ConfigAccessor getSecondary() {
        return secondary;
    }

    private void checkNodesFromJarFile(String path, ConfigAccessor config) {
        InputStream stream = Languages.getInstance().getResource(path);
        if (stream == null) return;
        ConfigAccessor jarConfig = new ConfigAccessor(stream);
        jarConfig.getKeys(true).stream().filter(node -> !config.contains(node)).forEach(node -> config.set(node, jarConfig.getString(node)));
        config.getKeys(true).stream().filter(node -> !jarConfig.contains(node)).forEach(node -> config.set(node, null));
        config.save();
    }
}
