package com.degoos.languages.api;

import com.degoos.languages.Languages;
import com.degoos.languages.enums.Language;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.PlayerManager;
import com.degoos.languages.manager.PluginManager;
import com.degoos.languages.util.LanguageUtils;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.plugin.WSPlugin;
import com.degoos.wetsponge.text.WSText;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LanguagesAPI {

	public static void addPlugin(WSPlugin plugin) {
		ManagerLoader.getManager(PluginManager.class).addPlugin(plugin);
	}

	public static void sendMessage(WSCommandSource source, String node, WSPlugin plugin, Object... args) {
		sendMessage(source, node, true, plugin, args);
	}

	public static void sendMessage(WSCommandSource source, String node, boolean prefix, WSPlugin plugin, Object... args) {
		sendMessage(source, node, prefix, false, plugin, args);
	}


	public static void sendMessage(WSCommandSource source, String node, boolean prefix, boolean center, WSPlugin plugin, Object... args) {
		boolean isPlayer = source instanceof WSPlayer;
		getMessage(isPlayer ? ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer((WSPlayer) source).getLanguage()
		                    : Languages.getDefaultLanguage(), node, prefix, center, plugin, isPlayer ? (WSPlayer) source : null, args).ifPresent(source::sendMessage);
	}


	public static Optional<WSText> getMessage(WSCommandSource source, String node, WSPlugin plugin, Object... args) {
		return getMessage(source, node, true, plugin, args);
	}

	public static Optional<WSText> getMessage(WSCommandSource source, String node, boolean prefix, WSPlugin plugin, Object... args) {
		return getMessage(source, node, prefix, false, plugin, args);
	}

	public static Optional<WSText> getMessage(WSCommandSource source, String node, boolean prefix, boolean center, WSPlugin plugin, Object... args) {
		boolean isPlayer = source instanceof WSPlayer;
		return getMessage(isPlayer ? ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer((WSPlayer) source).getLanguage()
		                           : Languages.getDefaultLanguage(), node, prefix, center, plugin, isPlayer ? (WSPlayer) source : null, args);
	}


	public static Optional<WSText> getMessage(Language language, String node, WSPlugin plugin, Object... args) {
		return getMessage(language, node, true, plugin, args);
	}

	public static Optional<WSText> getMessage(Language language, String node, boolean prefix, WSPlugin plugin, Object... args) {
		return getMessage(language, node, prefix, false, plugin, args);
	}

	public static Optional<WSText> getMessage(Language language, String node, boolean prefix, boolean center, WSPlugin plugin, Object... args) {
		return getMessage(language, node, prefix, center, plugin, null, args);
	}

	private static Optional<WSText> getMessage(Language language, String node, boolean prefix, boolean center, WSPlugin plugin, WSPlayer player, Object... args) {
		Language nearestLanguage = LanguageUtils.getNearestCompatibleLanguage(language, plugin);
		return nearestLanguage.getLanguageMapUnchecked(plugin).getMessage(node).filter(message -> !message.equals(""))
			.map(message -> LanguageUtils.factMessage(message, nearestLanguage, plugin, prefix, center, player, args));
	}


	public static List<WSText> getMessages(WSCommandSource source, String node, WSPlugin plugin, Object... args) {
		return getMessages(source, node, true, plugin, args);
	}

	public static List<WSText> getMessages(WSCommandSource source, String node, boolean prefix, WSPlugin plugin, Object... args) {
		return getMessages(source, node, prefix, false, plugin, args);
	}

	public static List<WSText> getMessages(WSCommandSource source, String node, boolean prefix, boolean center, WSPlugin plugin, Object... args) {
		boolean isPlayer = source instanceof WSPlayer;
		return getMessages(isPlayer ? ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer((WSPlayer) source).getLanguage()
		                            : Languages.getDefaultLanguage(), node, prefix, center, plugin, isPlayer ? (WSPlayer) source : null, args);
	}


	public static List<WSText> getMessages(Language language, String node, WSPlugin plugin, Object... args) {
		return getMessages(language, node, true, plugin, args);
	}

	public static List<WSText> getMessages(Language language, String node, boolean prefix, WSPlugin plugin, Object... args) {
		return getMessages(language, node, prefix, false, plugin, args);
	}

	public static List<WSText> getMessages(Language language, String node, boolean prefix, boolean center, WSPlugin plugin, Object... args) {
		return getMessages(language, node, prefix, center, plugin, null, args);
	}

	private static List<WSText> getMessages(Language language, String node, boolean prefix, boolean center, WSPlugin plugin, WSPlayer player, Object... args) {
		Language nearestLanguage = LanguageUtils.getNearestCompatibleLanguage(language, plugin);
		String[] split = nearestLanguage.getLanguageMapUnchecked(plugin).getMessage(node).orElse("").split("\\n");
		return Arrays.stream(split).filter(string -> !string.equals(""))
			.map(message -> LanguageUtils.factMessage(message, nearestLanguage, plugin, prefix, center, player, args)).collect(Collectors.toList());
	}

}
