package com.degoos.languages.object;

import com.degoos.languages.enums.Language;
import java.util.UUID;

public class LoginPlayer {

	private Language language;
	private boolean fullyLoaded;
	private UUID uuid;
	private LangPlayerRecord record;

	public LoginPlayer(Language language, boolean fullyLoaded, UUID uuid, LangPlayerRecord record) {
		this.language = language;
		this.fullyLoaded = fullyLoaded;
		this.uuid = uuid;
		this.record = record;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public boolean isFullyLoaded() {
		return fullyLoaded;
	}

	public void setFullyLoaded(boolean fullyLoaded) {
		this.fullyLoaded = fullyLoaded;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public LangPlayerRecord getRecord() {
		return record;
	}

	public void setRecord(LangPlayerRecord record) {
		this.record = record;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		LoginPlayer that = (LoginPlayer) o;

		return uuid != null ? uuid.equals(that.uuid) : that.uuid == null;
	}

	@Override
	public int hashCode() {
		return uuid != null ? uuid.hashCode() : 0;
	}
}
