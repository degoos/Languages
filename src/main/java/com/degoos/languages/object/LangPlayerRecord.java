package com.degoos.languages.object;


import com.degoos.languages.Languages;
import com.degoos.languages.enums.EnumLanguageChangeReason;
import com.degoos.languages.enums.Language;
import com.degoos.languages.manager.DatabaseManager;
import com.degoos.wetsponge.WetSponge;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import javax.persistence.Column;
import java.sql.Connection;
import java.util.UUID;

/**
 * Languages on 26/06/2017 by IhToN.
 */
public class LangPlayerRecord {
	@Column (name = "UUID")
	public String uuid;

	@Column (name = "playerName")
	public String playerName;

	@Column (name = "langCode")
	public String langCode;


	public LangPlayerRecord (String uuid, String playerName, String langCode) {
		this.uuid = uuid;
		this.playerName = playerName;
		this.langCode = langCode;
	}


	public LanguagePlayer createLanguagePlayer () {
		return new LanguagePlayer(this, WetSponge.getServer().getPlayer(UUID.fromString(uuid)).orElseThrow(NullPointerException::new),
		                          Language.getByLocaleCode(langCode).orElse(Languages.getDefaultLanguage()));
	}


	public void createLanguagePlayer (LanguagePlayer player) {
		player.setLanguage(Language.getByLocaleCode(langCode).orElse(Languages.getDefaultLanguage()), EnumLanguageChangeReason.JOIN);
		player.setLpRecord(this);
	}

	public void updateData(LanguagePlayer lp) {
		this.playerName = lp.getPlayer().getName();
		this.langCode = lp.getLanguage().getLocaleCode();
	}


	public void update () {
		new Thread(() -> {
			Connection connection = DatabaseManager.getConnection();
			if (connection == null) return;
			DSLContext create = DSL.using(connection);

			// MySQL
			switch (create.family()) {

				case H2:
					create.mergeInto(DSL.table(Languages.TABLE), DSL.field("UUID"), DSL.field("playerName"), DSL.field("langCode"))
							.key(DSL.field("UUID"))
							.values(this.uuid, this.playerName, this.langCode)
							.execute();
					break;
				case SQLITE:
					create.execute("INSERT OR REPLACE INTO " +
					               Languages.TABLE +
					               " (UUID, playerName, langCode) VALUES (  '" +
					               this.uuid +
					               "', '" +
					               this.playerName +
					               "', '" +
					               this.langCode +
					               "' );");
					break;
				case CUBRID:
				case HSQLDB:
				case MARIADB:
				case MYSQL:
				default:
					create.insertInto(DSL.table(Languages.TABLE), DSL.field("UUID"), DSL.field("playerName"), DSL.field("langCode"))
							.values(this.uuid, this.playerName, this.langCode)
							.onDuplicateKeyUpdate()
							.set(DSL.field("playerName"), this.playerName)
							.set(DSL.field("langCode"), this.langCode)
							.execute();
					break;

			}
			DatabaseManager.sendCommit();
		}).start();
	}
}
