package com.degoos.languages.object;


import com.degoos.languages.enums.EnumLanguageChangeReason;
import com.degoos.languages.enums.Language;
import com.degoos.languages.event.LanguageChangeEvent;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.data.WSTransaction;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.util.Validate;

public class LanguagePlayer {

	private LangPlayerRecord lpRecord;
	private WSPlayer         player;
	private Language         language;


	public LanguagePlayer (LangPlayerRecord lpRecord, WSPlayer player, Language language) {
		Validate.notNull(player, "Player cannot be null!");
		Validate.notNull(language, "Language cannot be null!");
		this.lpRecord = lpRecord;
		this.player = player;
		this.language = language;
	}


	public WSPlayer getPlayer () {
		return player;
	}


	public Language getLanguage () {
		return language;
	}


	public void setLanguage (Language language, EnumLanguageChangeReason reason) {

		LanguageChangeEvent languageChangeEvent = new LanguageChangeEvent(reason, new WSTransaction<>(this.language, language), this);
		WetSponge.getEventManager().callEvent(languageChangeEvent);
		if (languageChangeEvent.isCancelled()) return;
		this.language = languageChangeEvent.getTransaction().getNewData();
		if (this.lpRecord != null) {
			this.lpRecord.updateData(this);
			this.lpRecord.update();
		}
	}


	public LangPlayerRecord getLpRecord () {
		return lpRecord;
	}


	public void setLpRecord (LangPlayerRecord lpRecord) {
		this.lpRecord = lpRecord;
	}


	@Override
	public boolean equals (Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		LanguagePlayer that = (LanguagePlayer) o;

		return player.equals(that.player);
	}


	@Override
	public int hashCode () {
		return player.hashCode();
	}
}
