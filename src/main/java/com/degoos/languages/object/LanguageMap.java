package com.degoos.languages.object;

import com.degoos.wetsponge.config.ConfigAccessor;
import com.degoos.wetsponge.plugin.WSPlugin;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LanguageMap {

    private WSPlugin plugin;
    private Map<String, String> messages;

    public LanguageMap(WSPlugin plugin, ConfigAccessor langFile) {
        this.plugin = plugin;
        this.messages = new HashMap<>();
        langFile.getKeys(true).forEach(node -> messages.put(node, langFile.getString(node)));
    }


    public WSPlugin getPlugin() {
        return plugin;
    }


    public Map<String, String> getMessages() {
        return new HashMap<>();
    }

    public Optional<String> getMessage(String node) {
        return Optional.ofNullable(messages.get(node));
    }

    public void addMessage(String node, String message) {
        if (messages.containsKey(node)) removeMessage(node);
        messages.put(node, message);
    }

    public void removeMessage(String node) {
        messages.remove(node);
    }

    public boolean hasMessage(String node) {
        return messages.containsKey(node);
    }

}
