package com.degoos.languages.command;

import com.degoos.languages.Languages;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.languages.enums.EnumLanguageChangeReason;
import com.degoos.languages.enums.Language;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.PlayerManager;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SetLanguageCommand extends WSCommand {

    public SetLanguageCommand() {
        super("setLanguage", "Allows you to set your language.", "setLang");
    }

    @Override
    public void executeCommand(WSCommandSource source, String command, String[] arguments) {
        if (!source.hasPermission("languages.setLanguage")) {
            LanguagesAPI.sendMessage(source, "error.notEnoughPermission", Languages.getInstance());
            return;
        }
        if (!(source instanceof WSPlayer)) {
            LanguagesAPI.sendMessage(source, "error.notPlayer", Languages.getInstance());
            return;
        }
        if (arguments.length != 1) {
            LanguagesAPI.sendMessage(source, "command.setLanguage.usage", Languages.getInstance());
            return;
        }
        arguments[0] = arguments[0].replace("-", " ");;
        Optional<Language> optional = Language.getByLocaleCode(arguments[0]);
        if (!optional.isPresent()) {
            optional = Language.getByName(arguments[0]);
            if (!optional.isPresent()) {
                LanguagesAPI.sendMessage(source, "error.notLanguage", Languages.getInstance());
                return;
            }
        }

        Language language = optional.get();

        ManagerLoader.getManager(PlayerManager.class).getOrCreatePlayer((WSPlayer) source).setLanguage(language, EnumLanguageChangeReason.COMMAND);
        LanguagesAPI.sendMessage(source, "command.setLanguage.done", Languages.getInstance(), "<LANGUAGE>",
                LanguagesAPI.getMessage(source, "language." + language.getLocaleCode(), false,
                        Languages.getInstance()).map(WSText::toFormattingText).orElse(language.getDefaultName()));
    }

    @Override
    public List<String> sendTab(WSCommandSource source, String command, String[] arguments) {
        if (!source.hasPermission("languages.setLanguage")) return new ArrayList<>();
        switch (arguments.length) {
            case 1:
                return Language.getAllNames().stream().filter(arg -> arg.toLowerCase().startsWith(arguments[0].toLowerCase())).map(s -> s.replace(" ", "-"))
                        .collect(Collectors.toList());
            default:
                return new ArrayList<>();
        }
    }
}
