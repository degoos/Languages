package com.degoos.languages.command;

import com.degoos.languages.Languages;
import com.degoos.languages.api.LanguagesAPI;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.PluginManager;
import com.degoos.wetsponge.command.WSCommand;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LanguagesCommand extends WSCommand {

    private static final List<String> args0 = Arrays.asList("plugins", "reload");

    public LanguagesCommand() {
        super("languages", "Admin command of the plugin Languages.", "langs");
    }

    @Override
    public void executeCommand(WSCommandSource source, String command, String[] args) {
        if (!source.hasPermission("languages.admin")) LanguagesAPI.sendMessage(source, "error.notEnoughPermission", Languages.getInstance());
        switch (args.length) {
            case 1:
                switch (args[0].toLowerCase()) {
                    case "plugins":
                        sendPluginList(source);
                        break;
                    case "reload":
                        ManagerLoader.getManager(PluginManager.class).load();
                        LanguagesAPI.sendMessage(source, "command.reload.done", Languages.getInstance());
                        break;
                    default:
                        sendHelp(source);
                        break;
                }
                break;
            default:
                sendHelp(source);
                break;
        }
    }

    @Override
    public List<String> sendTab(WSCommandSource source, String command, String[] args) {
        if (!source.hasPermission("languages.admin")) return new ArrayList<>();
        switch (args.length) {
            case 1:
                return args0.stream().filter(arg -> arg.toLowerCase().startsWith(args[0].toLowerCase())).collect(Collectors.toList());
            default:
                return new ArrayList<>();
        }
    }


    public void sendHelp(WSCommandSource source) {
        LanguagesAPI.sendMessage(source, "command.help.header", false, true, Languages.getInstance());
        args0.forEach(arg -> LanguagesAPI.getMessage(source, "command.help." + arg, false, Languages.getInstance())
                .ifPresent(message -> source.sendMessage(message.toBuilder().hoverAction(WSShowTextAction.
                        of(LanguagesAPI.getMessage(source, "command.help." + arg + "HoverEvent",
                                false, Languages.getInstance()).orElse(WSText.empty()))).build())));
        LanguagesAPI.sendMessage(source, "command.help.footer", false, true, Languages.getInstance());
    }

    public void sendPluginList(WSCommandSource source) {
        LanguagesAPI.sendMessage(source, "command.plugins.header", false, true, Languages.getInstance());

        ManagerLoader.getManager(PluginManager.class).getPlugins().forEach((plugin, list) -> {
            WSText.Builder hoverEventText = LanguagesAPI.getMessage(source, "command.plugins.pluginHoverEvent", false,
                    Languages.getInstance(), "<NAME>", plugin.getId()).orElse(WSText.empty()).toBuilder();
            list.forEach(language -> hoverEventText.newLine().append(LanguagesAPI
                    .getMessage(source, "command.plugins.compatibleLanguage", false, Languages.getInstance(), "<LANGUAGE>",
                            language.getDefaultName(), "<CODE>", language.getLocaleCode()).orElse(WSText.empty())));
            LanguagesAPI.getMessage(source, "command.plugins.plugin", false, Languages.getInstance(), "<PLUGIN>", plugin.getId())
                    .ifPresent(message -> source.sendMessage(message.toBuilder().hoverAction(WSShowTextAction.of(hoverEventText.build())).build()));
        });

        LanguagesAPI.sendMessage(source, "command.plugins.footer", false, true, Languages.getInstance());
    }
}
