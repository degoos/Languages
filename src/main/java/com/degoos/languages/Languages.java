package com.degoos.languages;


import com.degoos.languages.command.LanguagesCommand;
import com.degoos.languages.command.SetLanguageCommand;
import com.degoos.languages.enums.Language;
import com.degoos.languages.listener.Packet2Listener;
import com.degoos.languages.listener.PacketListener;
import com.degoos.languages.listener.PlayerListener;
import com.degoos.languages.loader.ManagerLoader;
import com.degoos.languages.manager.DatabaseManager;
import com.degoos.languages.manager.FileManager;
import com.degoos.languages.manager.Manager;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.plugin.WSPlugin;

import java.sql.SQLException;

public class Languages extends WSPlugin {

    private static Languages instance;
    public static final String TABLE = "languages_users";
    private static Language defaultLanguage;


    @Override
    public void onEnable() {
        instance = this;
        ManagerLoader.load();
        defaultLanguage = Language.getByLocaleCode(ManagerLoader.getManager(FileManager.class).getConfig().getString("defaultLanguage"))
                .orElse(Language.AMERICAN_ENGLISH);
        WetSponge.getEventManager().registerListener(new PlayerListener(), this);

        boolean commandsEnabled = ManagerLoader.getManager(FileManager.class).getConfig().getBoolean("commandsEnabled", true);
        if (commandsEnabled) {
            WetSponge.getCommandManager().addCommand(new LanguagesCommand());
            WetSponge.getCommandManager().addCommand(new SetLanguageCommand());
        }

        try {
            Class.forName("com.degoos.wetsponge.packet.play.server.WSSPacketSetSlot");
            WetSponge.getEventManager().registerListener(new PacketListener(), this);
        } catch (Exception ignore) {
        }
        try {
            Class.forName("com.degoos.wetsponge.packet.play.server.WSSPacketOpenWindow");
            WetSponge.getEventManager().registerListener(new Packet2Listener(), this);
        } catch (Exception ignore) {

        }
    }

    @Override
    public void onDisable() {
        try {
            DatabaseManager.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Languages getInstance() {
        return instance;
    }


    public static <T extends Manager> T getManager(Class<T> clazz) {
        return ManagerLoader.getManager(clazz);
    }

    public static Language getDefaultLanguage() {
        return defaultLanguage;
    }
}

