-- Create table
CREATE TABLE IF NOT EXISTS user_languages (
  UUID        VARCHAR(255) PRIMARY KEY,
  playerName  VARCHAR(255),
  langCode    VARCHAR(10)
);

-- Select query
SELECT langCode FROM user_languages WHERE UUID = 'UUIDHERE';

-- Update query
UPDATE user_languages SET playerName = 'PLAYERNAMEHERE', langCode = 'LANGCODEHERE' WHERE UUID = 'UUIDHERE';